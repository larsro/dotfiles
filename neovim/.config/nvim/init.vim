call plug#begin()
  Plug 'arcticicestudio/nord-vim'
  Plug 'itchyny/lightline.vim'
  Plug 'tpope/vim-sensible'
  Plug 'ntpeters/vim-better-whitespace'
call plug#end()
set nocompatible
if has('autocmd')
  filetype plugin indent on
endif
if has('syntax')
  syntax enable
endif
set autoindent
set backspace=indent,eol,start
set complete-=i
set smarttab
set nrformats-=octal
set ttimeout
set ttimeoutlen=100
set incsearch
set tabstop=2
set shiftwidth=2
set expandtab
nnoremap <silent> <C-L> :nohlsearch<CR><C-L>
nnoremap <silent> <F5> :StripWhitespace<CR>
nnoremap <C-Left> :tabprevious<CR>
nnoremap <C-Right> :tabnext<CR>
nnoremap <C-E> :Tex<CR><C-E>
nnoremap <C-T> :tabnew<CR>
nnoremap <C-X> :bd<CR>
"nnoremap <silent> <F5> :let _s=@/<Bar>:%s/\s\+$//e<Bar>:let @/=_s<Bar>:nohl<CR>
command W80 :%s/\(.\{80\}\)/\1\r/g
command W120 :%s/\(.\{120\}\)/\1\r/g
command W132 :%s/\(.\{132\}\)/\1\r/g
set laststatus=2
set ruler
set showcmd
set wildmenu
set display+=lastline
set encoding=utf-8
set fileformats+=mac
set history=1000
set tabpagemax=50
set cursorline
set scrolloff=8
set sidescroll=1
set sidescrolloff=15
set noerrorbells
set visualbell
set hlsearch
set ignorecase
set noshowmode
set smartcase
set title
set shortmess+=I
set clipboard=unnamed
set number
set colorcolumn=80
autocmd FileType cobol set colorcolumn=7,72
set guifont=Cousine:h11
if (has("termguicolors"))
 set termguicolors
endif
colorscheme nord
let g:lightline = {
  \ 'colorscheme': 'nord',
  \ }
let g:better_whitespace_enabled=1
let g:strip_whitespace_on_save=1
let g:netrw_banner = 0
"highlight ExtraWhitespace ctermbg=red guibg=red
"autocmd FileType cobol match ExtraWhitespace '\%>72v.\+'
"match ExtraWhitespace '\s\+$'
"set diffopt+=iwhite
set nobackup
set nowritebackup
set noswapfile
set nowrap
