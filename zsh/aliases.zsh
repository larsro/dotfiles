# neovim
alias vi='nvim'
alias vim='nvim'
alias svim='sudo nvim'

# zsh
alias zshrc='vim ~/.zshrc'

# various
alias hs='history | grep'
alias myip="curl http://ipecho.net/plain; echo"