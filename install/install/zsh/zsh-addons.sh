#!/bin/sh

# Install pure
git clone https://github.com/sindresorhus/pure.git
mkdir -p $HOME/.zfunctions
ln -s "$PWD/pure/pure.zsh" "$HOME/.zfunctions/prompt_pure_setup"
ln -s "$PWD/pure/async.zsh" "$HOME/.zfunctions/async"
echo "fpath=( "$HOME/.zfunctions" $fpath )" >> ${ZDOTDIR:-$HOME}/.zshrc
echo "autoload -U promptinit; promptinit" >> ${ZDOTDIR:-$HOME}/.zshrc
echo "prompt pure" >> ${ZDOTDIR:-$HOME}/.zshrc

# Install zsh-syntax-highlighting
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git
echo "source ${(q-)PWD}/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh" >> ${ZDOTDIR:-$HOME}/.zshrc